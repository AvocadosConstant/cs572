import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import java.util.LinkedList;

public class Jpp extends DecafBaseListener {
  public int indent = 0;

  // Queue of comments that haven't been printed
  public LinkedList<Token> commentQueue;
  public CommonTokenStream tokens;

  // Initializes a StringBuilder with the right amount of indentation
  public StringBuilder indent() {
    StringBuilder out = new StringBuilder();
    for (int i = 0; i < indent; i++) {
      out.append("  ");
    }
    return out;
  }
  
  // Prints all comments in commentQueue until current token
  public void printCommentsUntilCur(ParserRuleContext ctx) {
    int curIdx = ctx.getStart().getTokenIndex();
    while (commentQueue.peek() != null &&
           commentQueue.peek().getTokenIndex() < curIdx) {
      StringBuilder out = indent().append(commentQueue.poll().getText());
      if (out.charAt(out.length() - 2) == '*') {
        // current comment is a block comment
        out.append("\n");
      }
      System.out.print(out);
    }
  }

  @Override
  public void enterId(DecafParser.IdContext ctx) {
    System.out.print(ctx.getChild(0));
    System.out.print(" ");
  }

  @Override
  public void enterClass_rule(DecafParser.Class_ruleContext ctx) {    
    printCommentsUntilCur(ctx);
    System.out.print(indent().append("class "));
    indent++;
  }

  @Override
  public void exitClass_rule(DecafParser.Class_ruleContext ctx) {    
    indent--;
  }

  @Override
  public void enterExtends_rule(DecafParser.Extends_ruleContext ctx) {
    System.out.print("extends ");
  }

  @Override
  public void enterClass_block(DecafParser.Class_blockContext ctx) {
    System.out.println("{");
  }

  @Override
  public void exitClass_block(DecafParser.Class_blockContext ctx) {
    indent--;
    System.out.println(indent().append("}"));
    indent++;
  }

  @Override
  public void enterDimension(DecafParser.DimensionContext ctx) {
    System.out.print("[ ");
  }

  @Override
  public void exitDimension(DecafParser.DimensionContext ctx) {
    System.out.print("] ");
  }

  @Override
  public void enterField(DecafParser.FieldContext ctx) {    
    printCommentsUntilCur(ctx);
    System.out.print(indent());
  }

  @Override
  public void exitField(DecafParser.FieldContext ctx) {    
    System.out.println(";");
  }

  @Override
  public void enterMethod(DecafParser.MethodContext ctx) {
    printCommentsUntilCur(ctx);
    System.out.print(indent());
    indent++;
  }

  @Override
  public void exitMethod(DecafParser.MethodContext ctx) {    
    indent--;
  }

  @Override
  public void enterCtor(DecafParser.CtorContext ctx) {
    printCommentsUntilCur(ctx);
    System.out.print(indent());
    indent++;
  }

  @Override
  public void exitCtor(DecafParser.CtorContext ctx) {    
    indent--;
  }

  @Override
  public void enterModifier(DecafParser.ModifierContext ctx) {
    System.out.print(ctx.getChild(0));
    System.out.print(" ");
  }

  @Override
  public void enterFormalArgs(DecafParser.FormalArgsContext ctx) {
    System.out.print("( ");
  }

  @Override
  public void exitFormalArgs(DecafParser.FormalArgsContext ctx) {
    System.out.print(") ");
  }

  @Override
  public void exitFormalArg(DecafParser.FormalArgContext ctx) {
    if (ctx.getParent().getChildCount() > 1) {
      System.out.print(", ");
    }
  }

  @Override
  public void exitType(DecafParser.TypeContext ctx) {
    if (ctx.getChildCount() == 3) {
      System.out.print("[ ] ");
    }
  }

  @Override
  public void enterPrimitiveType(DecafParser.PrimitiveTypeContext ctx) {
    System.out.print(ctx.getChild(0));
    System.out.print(" ");
  }

  @Override
  public void exitVarDeclaratorId(DecafParser.VarDeclaratorIdContext ctx) {
    if (ctx.getChildCount() == 3) {
      System.out.print("[ ] ");
    }
  }
  
  @Override
  public void enterBlock(DecafParser.BlockContext ctx) {
    System.out.println("{");
  }

  @Override
  public void exitBlock(DecafParser.BlockContext ctx) {
    indent--;
    System.out.println(indent().append("}"));
    indent++;
  }

  @Override
  public void enterStatement(DecafParser.StatementContext ctx) {
    printCommentsUntilCur(ctx);
  }

  @Override
  public void enterStatSemi(DecafParser.StatSemiContext ctx) {
    StringBuilder out = indent().append(";");
    System.out.println(out);
  }

  @Override
  public void enterStatDecl(DecafParser.StatDeclContext ctx) {
    StringBuilder out = indent();
    System.out.print(out);
  }

  @Override
  public void exitStatDecl(DecafParser.StatDeclContext ctx) {
    System.out.println(";");
  }

  @Override
  public void enterStatIf(DecafParser.StatIfContext ctx) {
    System.out.print(indent().append("if "));
    indent++;
  }

  @Override
  public void exitStatIf(DecafParser.StatIfContext ctx) {
    indent--;
  }

  @Override
  public void enterStatIfElse(DecafParser.StatIfElseContext ctx) {
    System.out.print(indent().append("if "));
    indent++;
  }

  @Override
  public void enterStatWhile(DecafParser.StatWhileContext ctx) {
    System.out.print(indent().append("while "));
    indent++;
  }

  @Override
  public void exitStatWhile(DecafParser.StatWhileContext ctx) {
    indent--;
  }

  @Override
  public void enterStatExpr(DecafParser.StatExprContext ctx) {
    StringBuilder out = indent();
    System.out.print(out);
  }

  @Override
  public void exitStatExpr(DecafParser.StatExprContext ctx) {
    System.out.println(";");
  }

  @Override
  public void enterStatReturn(DecafParser.StatReturnContext ctx) {
    System.out.print(indent().append("return "));
  }

  @Override
  public void exitStatReturn(DecafParser.StatReturnContext ctx) {
    System.out.println(";");
  }

  @Override
  public void enterStatContBreak(DecafParser.StatContBreakContext ctx) {
    System.out.println(
      indent().append(ctx.getChild(0).getText()).append(" ;"));
  }

  @Override
  public void enterStatSuper(DecafParser.StatSuperContext ctx) {
    System.out.print(indent().append("super "));
  }

  @Override
  public void exitStatSuper(DecafParser.StatSuperContext ctx) {
    System.out.println(";");
  }

  @Override
  public void enterElse_rule(DecafParser.Else_ruleContext ctx) {
    indent--;
    StringBuilder out = indent().append("else ");
    System.out.print(out);
    if (!(ctx.getChild(1).getChild(0) instanceof DecafParser.BlockContext)) {
      // If there is no block after the else
      System.out.println();
    }
    indent++;
  }

  @Override
  public void exitElse_rule(DecafParser.Else_ruleContext ctx) {
    indent--;
  }

  @Override
  public void enterConditional(DecafParser.ConditionalContext ctx) {
    System.out.print("( ");
  }

  @Override
  public void exitConditional(DecafParser.ConditionalContext ctx) {
    System.out.print(") ");
    if (!(ctx.getParent().getChild(2).getChild(0) instanceof DecafParser.StatBlockContext)) {
      // If there is no block after the else
      System.out.println();
      //System.out.println("|NO BLOCK AFTER HERE:" + ctx.getParent().getChild(2).getChild(0).getClass().getName());
    }
  }

  @Override
  public void enterExpression(DecafParser.ExpressionContext ctx) {
  }

  @Override
  public void exitExpression(DecafParser.ExpressionContext ctx) {
    if (ctx.getParent() instanceof DecafParser.ExprListContext &&
    ctx.getParent().getChildCount() == 3) {
      System.out.print(", ");
        
    }
  }

  @Override
  public void enterBinaryOp(DecafParser.BinaryOpContext ctx) {
    System.out.print(ctx.getChild(0) + " ");
  }

  @Override
  public void enterUnaryOp(DecafParser.UnaryOpContext ctx) {
    System.out.print(ctx.getChild(0) + " ");
  }

  @Override
  public void enterPrimary(DecafParser.PrimaryContext ctx) {
  }

  @Override
  public void enterNewArrayExpr(DecafParser.NewArrayExprContext ctx) {
    System.out.print("NEWARREXPR:");
  }

  @Override
  public void enterNonNewArrayExpr(DecafParser.NonNewArrayExprContext ctx) {
  }

  @Override
  public void enterLiteral(DecafParser.LiteralContext ctx) {
    System.out.print(ctx.getChild(0).getText());
    System.out.print(" ");
  }

  @Override
  public void enterActualArgs(DecafParser.ActualArgsContext ctx) {
    System.out.print("( ");
  }

  @Override
  public void exitActualArgs(DecafParser.ActualArgsContext ctx) {
    System.out.print(") ");
  }

  @Override
  public void enterExprList(DecafParser.ExprListContext ctx) {
    //System.out.print("( ");
  }

  public Jpp(CommonTokenStream tokens) {
    this.tokens = tokens;

    commentQueue = new LinkedList<>();
    //System.out.println("\nCHAN\tINDEX\tTEXT");
    for (Token token : tokens.getTokens()) {
      /*
      System.out.println(
        token.getChannel() +
        "\t" +
        token.getTokenIndex() +
        "\t" +
        token.getText());
      */
      if (token.getChannel() == 1) {
        commentQueue.add(token);
      }
    }

    /*
    System.out.println("\nINDEX\tTEXT");
    for (Token token : commentQueue) {
      System.out.println(
        token.getTokenIndex() +
        "\t" +
        token.getText());
    }
    System.out.println("\n\n");
    */
  }

  public static void main(String[] args) throws Exception {
    ANTLRInputStream input = new ANTLRInputStream(System.in);
    DecafLexer lexer = new DecafLexer(input);
    lexer.removeErrorListeners();
    CountingErrorListener errors = CountingErrorListener.INSTANCE;
    lexer.addErrorListener(errors);
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    DecafParser parser = new DecafParser(tokens);
    parser.removeErrorListeners();
    parser.addErrorListener(errors);

    // if using a listener
    ParseTree tree = parser.start();
    if (true) { // check for no errors
      Jpp listener = new Jpp(tokens);
      ParseTreeWalker walker = new ParseTreeWalker();
      walker.walk(listener, tree);
    }

  } // end main
} // end class
