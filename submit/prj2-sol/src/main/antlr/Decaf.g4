grammar Decaf;

WS
  : [ \t\r\n]+ -> skip
  ;
COMMENT
  : ( '//' ~[\r\n]* '\r'? '\n'
    | '/*' .*? '*/'
    ) -> channel(HIDDEN)
  ;
start
  : class_rule+
  ;
id
  : ID
  ;
ID
  : [a-zA-Z_] [a-zA-Z0-9_]*
  ;
class_rule
  : 'class' id extends_rule? class_block
  ;
extends_rule
  : 'extends' id
  ;
class_block
  : '{' member* '}'
  ;
member
  : field
  | method
  | ctor
  ;
field
  : modifier* type varDeclaratorList ';'
  ;
method
  : modifier* type id formalArgs block
  ;
ctor
  : modifier* id formalArgs block
  ;
modifier
  : 'static'
  | 'public'
  | 'private'
  | 'protected'
  ;
formalArgs
  : '(' formalArgList? ')'
  ; 
formalArgList
  : formalArg 
  | formalArg ',' formalArgList
  ;
formalArg
  : type varDeclaratorId
  ;
type
  : primitiveType
  | id
  | type '[' ']'
  ;
primitiveType
  : 'boolean'
  | 'char'
  | 'int'
  | 'void'
  ;
varDeclaratorList
  : varDeclarator ',' varDeclaratorList
  | varDeclarator
  ;
varDeclarator
  : varDeclaratorId
  | varDeclaratorId '=' expression
  ;
varDeclaratorId
  : id 
  | varDeclaratorId '[' ']'
  ;
block
  : '{' statement* '}'
  ;
statement
  : real_statement
  ;
real_statement
  : ';'                                      # statSemi
  | type varDeclaratorList ';'               # statDecl
  | 'if' conditional statement               # statIf
  | 'if' conditional statement else_rule     # statIfElse
  | 'while' conditional statement            # statWhile
  | expression ';'                           # statExpr
  | 'return' expression? ';'                 # statReturn
  | ('continue' | 'break') ';'               # statContBreak
  | 'super' actualArgs ';'                   # statSuper
  | block                                    # statBlock
  ;
else_rule
  : 'else' statement
  ;
conditional
  : '(' expression ')'
  ;
expression
  : expression binaryOp expression
  | unaryOp expression
  | primary
  ;
binaryOp
  : '=' | '||' | '&&' | '==' | '!=' | '<' | '>' | '<=' | '>='
  | '+' | '-' | '*' | '/' | '%'
  ;
unaryOp
  : '+' | '-' | '!'
  ;
primary
  : newArrayExpr
  | nonNewArrayExpr
  | id 
  | primary '.' id
  | primary '.' id actualArgs
  ;
newArrayExpr
  : 'new' id dimension+
  | 'new' primitiveType dimension+
  ;
dimension
  : '[' expression ']'
  ;
nonNewArrayExpr
  : literal
  | 'this'
  | conditional 
  | 'new' id actualArgs
  | id actualArgs
  | 'super' '.' id actualArgs
  | arrayExpr
  | nonNewArrayExpr dimension
  | fieldExpr
  ;
fieldExpr
  : 'super' '.' id
  ;
arrayExpr
  : id dimension
  ;
literal
  : 'null' | 'true' | 'false' | INT_LITERAL | CHAR_LITERAL | STRING_LITERAL
  ;
INT_LITERAL
  : [0]
  | [1-9] [0-9]*
  ;
fragment SINGLE_QUOTE
  : '\''
  ;
CHAR_LITERAL
  : '\'' ~[\\'\r\n] '\''
  | '\'\\' . '\''
  ;
STRING_LITERAL
  : '"' (~[\r\n] | ('\\\\')* '\\"' )*? '"'
  ;
actualArgs
  : '(' exprList? ')'
  ;
exprList
  : expression
  | expression ',' exprList
  ;

