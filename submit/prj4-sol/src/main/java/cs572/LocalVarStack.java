package cs572;

import java.util.Stack;
import java.util.HashMap;

public class LocalVarStack {
  private int curOffset;

  private Stack<HashMap<String, Integer>> stack;

  public LocalVarStack(boolean isStatic) {
    curOffset = 0;
    stack = new Stack<>();

    this.newScope();
    if (!isStatic) {
      this.add("this");
    }
  }

  public void newScope() {
    stack.push(new HashMap<String, Integer>());
  }

  public void exitScope() {
    stack.pop();
  }

  public void add(String name) {
    stack.peek().put(name, curOffset++);

    System.out.println("LocalVarStack: " + stack.peek());
  }

  public int numLocalVars() {
    return stack.get(0).size();
  }

  public int find(String name) {
    return stack.peek().get(name);
  }
}
