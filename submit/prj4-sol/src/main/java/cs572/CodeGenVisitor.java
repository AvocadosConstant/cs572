package cs572;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.Arrays;
import java.util.stream.Collectors;

class CodeGenVisitor extends EmptyVisitor implements AstVisitorI {

  /** Directory where jasmin files generated */
  private final String _destDir;

  /** Use for generating labels */
  private final LabelGenerator _labelGen;

  /** Stack used for tracking current false/true labels for current visit */
  private final LabelStack _labels;

  /** Stack used for tracking current false/true labels for loops */
  private final LabelStack _loopLabels;

  /** Generator for a class; create at start of class visit and emit
   *  code at end of class visit.
   */
  private JvmGen _jvmGen = null;

  private LocalVarStack localVarStack;

  private boolean needsDefaultConstructor;

  CodeGenVisitor(String destDir) {
    _destDir = destDir;
    _labelGen = new LabelGenerator();
    _labels = new LabelStack();
    _loopLabels = new LabelStack();
  }

  public void visit(ProgramAst ast) {
    System.out.println("visit ProgramAst");
    System.out.printf("There are %d classes%n", ast.getNClasses());

    ast.getClasses().forEach(
      classAst->classAst.accept(this)
    );
  }

  public void visit(ClassAst ast) {
    System.out.printf("visit ClassAst named %s with %d declarations%n",
        ast.getName(), ast.getNDeclarations());

    needsDefaultConstructor = true;

    _jvmGen = new JvmGen(_destDir, ast.getName(), ast.getSuperClass());

    ast.getDeclarations().forEach(decl->{
      decl.accept(this);
    });

    if (needsDefaultConstructor) {
        /*
        .method public <init>()V
            .limit stack 1
            .limit locals 1
            aload_0
            invokespecial java/lang/Object/<init>()V
            return
        .end method
         */
      _jvmGen.add(".method", "public", "<init>()V");
      _jvmGen.add(".limit", "stack", 1);
      _jvmGen.add(".limit", "locals", 1);
      _jvmGen.add("aload_0");
      _jvmGen.add("invokespecial", "java/lang/Object/<init>()V");
      _jvmGen.add("return");
      _jvmGen.add(".end", "method");
    }

    _jvmGen.emit();
  }

  public String getBrackets(int rank) {
    char[] chars = new char[rank];
    Arrays.fill(chars, '[');
    return new String(chars);
  }

  public String parseType(AbstractTypeAst ast) {
    if (ast instanceof TypeAst) {
      switch (((TypeAst)ast).getBaseType()) {
        case "boolean":
          return "Z";
        case "int":
          return "I";
        case "char":
          return "C";
        case "void":
          return "V";
        case "Object":
          return "Ljava/lang/Object;";
        case "String":
          return "Ljava/lang/String;";
        default:
          return ((TypeAst)ast).getBaseType();
      }
    }
    return "V";
  }

  public void visit(FieldDeclarationAst ast) {
    System.out.printf("visit FieldDeclarationAst with %d initDeclarators%n", ast.getNInitDeclarators());
    ast.getInitDeclarators().forEach(decl->{
      System.out.printf("InitDeclaratorAst named %s with rank %d%n", decl.getName(), decl.getRank());

      _jvmGen.add(
        ".field",
        ast.getAccess(),
        decl.getName(),
        getBrackets(decl.getRank()) + parseType(ast.getTypeAst()));
    });
  }

  public void visit(MethodDeclarationAst ast) {
    System.out.printf("visit MethodDeclarationAst named %s with %d formals%n", ast.getName(), ast.getNFormals());

    MethodSymInfo method = ast.getSymInfo();

    localVarStack = new LocalVarStack(method.isStatic());

    String params = "(" + ast.getFormals().stream()
      .map(f->getBrackets(f.getTypeAst().getRank()) + parseType(f.getTypeAst()))
      .collect(Collectors.joining()) + ")";

    _jvmGen.add(
      ".method",
      ast.getAccess() + (method.isStatic() ? " static" : ""),
      ast.getName() + params + parseType(ast.getReturnType())
    );

    ast.getFormals().forEach(f->
      localVarStack.add(f.getInitDeclarator(0).getName()));

    _jvmGen.add(".limit", "stack", 20);
    int localsIndex = _jvmGen.add(".limit", "locals", 1);

    ast.getBody().accept(this);

    _jvmGen.backpatch(".limit", localsIndex, "locals", localVarStack.numLocalVars());

    _jvmGen.add("return");

    _jvmGen.add(".end", "method");
  }

  public void visit(BlockAst ast) {
    System.out.printf("visit BlockAst with %d statements%n", ast.getNStatements());
    ast.getStatements().forEach(s->s.accept(this));
  }

  public void visit(ControlAst ast) {
    System.out.printf("visit ControlAst %n");
  }

  public void visit(ExpStatementAst ast) {
    System.out.printf("visit ExpStatementAst of type %s%n", ast.getExp().getClass().getName());
    ast.getExp().accept(this);
  }

  public void visit(IfAst ast) {
    System.out.printf("visit IfAst %n");
  }

  public void visit(NilStatementAst ast) {
    System.out.printf("visit NilStatementAst %n");
  }

  public void visit(ReturnAst ast) {
    System.out.printf("visit ReturnAst %n");
  }

  public void visit(SuperConsAst ast) {
    System.out.printf("visit SuperConsAst %n");
  }

  public void visit(VarDeclarationAst ast) {
    System.out.printf("visit VarDeclarationAst %n");
    ast.getInitDeclarators().forEach(decl->{
      localVarStack.add(decl.getName());
      decl.getInitializer().accept(this);

      if (ast.getTypeAst().isPrimitiveType()) {
        if (ast.getTypeAst().isArrayType()) {
          _jvmGen.add("iastore", localVarStack.find(decl.getName()));
        } else {
          _jvmGen.add("istore", localVarStack.find(decl.getName()));
        }
        
      } else {
        if (ast.getTypeAst().isArrayType()) {
          _jvmGen.add("aastore", localVarStack.find(decl.getName()));
        } else {
          _jvmGen.add("astore", localVarStack.find(decl.getName()));
        }
      }
    });
  }

  public void visit(WhileAst ast) {
    System.out.printf("visit WhileAst %n");
  }


  // Expressions
  public void visit(AssignAst ast) {
    System.out.printf("visit AssignAst %n");
  }

  public void visit(LiteralAst ast) {
    System.out.printf("visit LiteralAst %n");

    String lit = ast.getLiteral();
    
    switch (ast.getPredefType()) {
      case BOOLEAN:
        _jvmGen.add("ldc", (lit.equals("true") ? 1 : 0));
        break;
      case CHAR:
        _jvmGen.add("ldc", lit.charAt(1));
        break;
      case INT:
        _jvmGen.add("ldc", Integer.parseInt(lit));
        break;
      case OBJECT:
        _jvmGen.add("ldc", "aconst_null");
        break;
      case STRING:
        _jvmGen.add("ldc", lit);
        break;
      case VOID:
        _jvmGen.add("ldc", "aconst_null");
        break;
    }
  }
  /*
  public void visit(THINGAst ast) {
    System.out.printf("visit THINGAst %n");
  }
  */

  private static class LabelGenerator {
    private int labelN = 1;
    Integer next() { return new Integer(labelN++); }
  }

  private static class LabelStack {
    private Stack<Integer> _trueLabels = new Stack<Integer>();
    private Stack<Integer> _falseLabels = new Stack<Integer>();
    Integer peekTrue() { return _trueLabels.peek(); }
    Integer peekFalse() { return _falseLabels.peek(); }
    void push(Integer trueLabel, Integer falseLabel) {
      _trueLabels.push(trueLabel); _falseLabels.push(falseLabel);
    }
    void push(Integer label) { push(label, label); }
    void pop() { _trueLabels.pop(); _falseLabels.pop(); }
  }


}
