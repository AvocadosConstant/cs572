#include "eval.h"

#include "lexer.h"


enum State { want_term, want_operator };

const EvalResult Eval::check_init_token(TokenKind terminator) {
  auto status = EvalStatus::ok;
  if (token_.kind == TokenKind::nl) {
    if (terminator == TokenKind::nl) {
      status = EvalStatus::empty;
    } else {
      status = EvalStatus::error;
    }
  } else if (token_.kind == TokenKind::eof) {
    status = EvalStatus::eof;
  }
  return EvalResult({status, 0});
}

const EvalResult Eval::evaluate(TokenKind terminator) {
  EvalResult init_check = check_init_token(terminator);
  if (init_check.status != EvalStatus::ok) return init_check;

  auto result = 0;
  auto state = State::want_term;
  auto sign = 1;
  auto is_error = false;  

  while (!is_error && token_.kind != terminator
      && token_.kind != TokenKind::eof) {

    switch (state) {
      case State::want_term: {
        state = State::want_operator;
        if (token_.kind == TokenKind::left) {
          token_ = lexer_.next_token();
          auto sub_expr = evaluate(TokenKind::right);
          if (sub_expr.status == EvalStatus::empty ||
              sub_expr.status == EvalStatus::error) {
            return sub_expr;
          }
          result += sign * sub_expr.result;
        } else if (token_.kind == TokenKind::integer) {
          result += sign * token_.value;
        } else {
            is_error = true;
        }
        break;
      }
      case State::want_operator: {
        state = State::want_term;
        is_error = (token_.kind != TokenKind::add &&
            token_.kind != TokenKind::sub);
        if (!is_error) {
          sign = (token_.kind == TokenKind::sub) ? -1 : +1;
        }
        break;
      }
    } //switch

    if (!is_error) token_ = lexer_.next_token();
  } //while 

  is_error = is_error || (state != State::want_operator) || token_.kind != terminator;
  return is_error
    ?  EvalResult({EvalStatus::error, 0})
    : EvalResult({EvalStatus::ok, result});
}
