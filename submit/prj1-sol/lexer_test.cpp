#include "lexer.h"

#include <string>

#include <gtest/gtest.h>

TEST(LexerTest, Eof) {
  Token eof(TokenKind::eof, "");
  EXPECT_EQ(eof, Lexer("").next_token());
  EXPECT_EQ(eof, Lexer(" \t ").next_token());
}

TEST(LexerTest, Newline) {
  Token nl(TokenKind::nl, "\n");
  EXPECT_EQ(nl, Lexer("\n").next_token());
  EXPECT_EQ(nl, Lexer(" \t \n").next_token());
}
  
TEST(LexerTest, Operators) {
  Token left(TokenKind::left, "(");
  EXPECT_EQ(left, Lexer("(").next_token());
  EXPECT_EQ(left, Lexer(" \t (").next_token());
  Token right(TokenKind::right, ")");
  EXPECT_EQ(right, Lexer(")").next_token());
  EXPECT_EQ(right, Lexer(" \t )").next_token());
}

TEST(LexerTest, Parentheses) {
  Token add(TokenKind::add, "+");
  EXPECT_EQ(add, Lexer("+").next_token());
  EXPECT_EQ(add, Lexer(" \t +").next_token());
  Token sub(TokenKind::sub, "-");
  EXPECT_EQ(sub, Lexer("-").next_token());
  EXPECT_EQ(sub, Lexer(" \t -").next_token());
}

TEST(LexerTest, Integers) {
  for (auto i : {0, 1, 12, 1<<16,}) {
    std::string s = std::to_string(i);
    Token expected(TokenKind::integer, s, i);
    EXPECT_EQ(expected, Lexer(s).next_token());
    EXPECT_EQ(expected,
	      Lexer(" \t " + s).next_token());
  }
}

TEST(LexerTest, Sequence1) {
  int i1 = 63;
  int i2 = 20;
  int i3 = 11;
  auto tokens = {
    Token(TokenKind::integer, std::to_string(i1), i1),
    Token(TokenKind::add, "+"),
    Token(TokenKind::integer, std::to_string(i2), i2),
    Token(TokenKind::sub, "-"),
    Token(TokenKind::integer, std::to_string(i3), i3),
    Token(TokenKind::nl, "\n"),
    Token(TokenKind::eof, ""),
  };
  std::string s;
  for (auto token : tokens) {
    s += " \t"; s += token.lexeme;
  }
  Lexer lexer = Lexer(s);
  int i = 0;
  for (auto token : tokens) {
    EXPECT_EQ(token, lexer.next_token()) <<
      "for index " << i;
    ++i;
  }
}

TEST(LexerTest, Sequence2) {
  int i1 = 1;
  int i2 = 2;
  int i3 = 3;
  int i4 = 4;
  auto tokens = {
    Token(TokenKind::integer, std::to_string(i1), i1),
    Token(TokenKind::sub, "-"),
    Token(TokenKind::left, "("),
    Token(TokenKind::integer, std::to_string(i2), i2),
    Token(TokenKind::sub, "-"),
    Token(TokenKind::left, "("),
    Token(TokenKind::integer, std::to_string(i3), i3),
    Token(TokenKind::sub, "-"),
    Token(TokenKind::integer, std::to_string(i4), i4),
    Token(TokenKind::right, ")"),
    Token(TokenKind::right, ")"),
    Token(TokenKind::nl, "\n"),
    Token(TokenKind::eof, ""),
  };
  std::string s;
  for (auto token : tokens) {
    s += " \t"; s += token.lexeme;
  }
  Lexer lexer = Lexer(s);
  int i = 0;
  for (auto token : tokens) {
    EXPECT_EQ(token, lexer.next_token()) <<
      "for index " << i;
    ++i;
  }
}

