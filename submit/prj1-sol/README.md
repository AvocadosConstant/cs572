# Parenthesized Sub-Expressions for a Basic Calculator Language
##### Binghamton University CS 572 - Compiler Design Project 1

- Name:	   Tim Hung
- B-Number:  B00518486
- Email:     thung1@binghamton.edu

----

## Lexer

Implemented two new TokenKinds in lexer to handle parentheses:
```c++
    TokenKind::left, TokenKind::right
```


## Evaluator

NextEvalResult() has been named to evaluate() for clarity and sane naming conventions.

evaluate() is now a recursive function that takes an optional TokenKind terminator parameter. The terminator specifies what is expected at the end of the current sub-expression.

 - e.g. For an entire line to be evaluated, the expected terminator will be of TokenKind::nl.
 - e.g. For a parenthesized sub-expression, the expected terminator will be of TokenKind::right.

The function will make a recursive call when encountering a deeper sub-expression to evaluate, i.e. when it finds an open parenthesis.


## Unit Tests

New unit tests have been written for both lexer and eval.


## Specification

```bash
$ ./tl0
>> (1)
1

>> 1 - (2 - 3)
2

>> 1 - (2 - (3 - 4))
-2

>> (1
syntax error
(1
  ^

>> (1))
syntax error
(1))
   ^

>> ()
syntax error
()
 ^

```
