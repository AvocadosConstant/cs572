#include "lexer.h"

#include <assert.h>
#include <ctype.h>

#include <iostream>

void Lexer::eat_whitespace() {
  while (index_ < size_ && buffer_[index_] != '\n' &&
	 isspace(buffer_[index_])) {
    ++index_;
  }
}

const Token Lexer::next_token() {
  Lexer::eat_whitespace();
  if (index_ >= size_) return Token(TokenKind::eof, "");

  char ch = buffer_[index_];
  if (isdigit(ch)) return read_integer_token();

  TokenKind kind;
  std::string lexeme = std::string(1, ch);

  switch (ch) {
    case '\n':  kind = TokenKind::nl;     break;
    case '+':   kind = TokenKind::add;    break;
    case '-':   kind = TokenKind::sub;    break;
    case '(':   kind = TokenKind::left;   break;
    case ')':   kind = TokenKind::right;  break;
    default:    kind = TokenKind::error;
                lexeme = buffer_.substr(index_, 1);
  }
  ++index_;
  return Token(kind, lexeme);
}

const Token Lexer::read_integer_token() {
  assert(isdigit(buffer_[index_]));

  size_t start = index_;
  while (index_ < size_ && isdigit(buffer_[index_])) {
    ++index_;
  }
  std::string lexeme = buffer_.substr(start, index_ - start);
  return Token(TokenKind::integer, lexeme, std::stoi(lexeme));
}

#ifdef TEST_LEXER

#include <fstream>

#include <errno.h>
#include <stdlib.h>
#include <string.h>

static void
tokenize_istream(std::istream &in, std::ostream &out)
{
  std::string line;
  while (std::getline(in, line)) {
    line += "\n";
    Lexer lexer(line);
    while (true) {
      Token token(lexer.next_token());
      out << token << std::endl;
      if (token.kind == TokenKind::eof) break;
    }
    line.clear();
  }
}

int
main(int argc, const char *argv[])
{
  if (argc == 1) {
    std::cerr << "usage: " << argv[0] << " FILE...\n"
	      << "specify FILE as - for stdin\n";
    exit(1);
  }
  
  for (int i = 1; i < argc; ++i) {
    bool is_stdin = (std::string(argv[i]) == "-");
    if (is_stdin)
      tokenize_istream(std::cin, std::cout);
    else {
      std::ifstream in(argv[i], std::ios::in);
      if (in.fail()) {
	std::cerr << "cannot read " << argv[i]
		  << ": " << strerror(errno) << std::endl;
	exit(1);
      }
      tokenize_istream(in, std::cout);
      in.close();
    }
  }    
  return 0;
}

#endif 
