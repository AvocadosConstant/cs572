#include "eval.h"
#include "lexer.h"

#include <string>

#include <gtest/gtest.h>

TEST(EvalTest, Eof) {
  Lexer lexer("   ");
  EvalResult result(Eval(lexer).evaluate());
  EXPECT_EQ(result.status, EvalStatus::eof);
}

TEST(EvalTest, Empty) {
  Lexer lexer("   \n");
  EvalResult result(Eval(lexer).evaluate());
  EXPECT_EQ(result.status, EvalStatus::empty);
}

TEST(EvalTest, Zero) {
  Lexer lexer("   0  \n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::ok);
  EXPECT_EQ(result.result, 0);
}

TEST(EvalTest, Add) {
  Lexer lexer(" \t  63 \t +2  \n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::ok);
  EXPECT_EQ(result.result, 65);
}

TEST(EvalTest, Sub) {
  Lexer lexer(" \t 63 \t -20 -1\n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::ok);
  EXPECT_EQ(result.result, 42);
}

TEST(EvalTest, Paren1) {
  Lexer lexer("   (  1)  \n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::ok);
  EXPECT_EQ(result.result, 1);
}

TEST(EvalTest, Paren2) {
  Lexer lexer("   ((( ( 1  )) ) )  \n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::ok);
  EXPECT_EQ(result.result, 1);
}

TEST(EvalTest, ParenError1) {
  Lexer lexer("   (x)  \n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::error);
}

TEST(EvalTest, ParenError2) {
  Lexer lexer("   )(  \n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::error);
}

TEST(EvalTest, ParenError3) {
  Lexer lexer("   (  \n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::error);
}

TEST(EvalTest, ParenError4) {
  Lexer lexer("   )  \n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::error);
}

TEST(EvalTest, ParenError5) {
  Lexer lexer("   ()  \n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::error);
}

TEST(EvalTest, ParenError6) {
  Lexer lexer("   ( 2  \n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::error);
}

TEST(EvalTest, ParenError7) {
  Lexer lexer("   (1))  \n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::error);
}

TEST(EvalTest, ParenError8) {
  Lexer lexer("   ( (1)  \n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::error);
}

TEST(EvalTest, ParenError9) {
  Lexer lexer("   1 + )  \n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::error);
}

TEST(EvalTest, ParenError10) {
  Lexer lexer("   1 + (  \n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::error);
}

TEST(EvalTest, ParenError11) {
  Lexer lexer("   ( + 1  \n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::error);
}

TEST(EvalTest, ParenError12) {
  Lexer lexer("   ) + 1  \n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::error);
}

TEST(EvalTest, SubExprs1) {
  Lexer lexer("   1 - (2 - (3 - 4))  \n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::ok);
  EXPECT_EQ(result.result, -2);
}

TEST(EvalTest, SubExprs2) {
  Lexer lexer("   1 - (2 + (3 - 4))  \n");
  EvalResult result(Eval(lexer).evaluate());
  ASSERT_EQ(result.status, EvalStatus::ok);
  EXPECT_EQ(result.result, 0);
}
