#ifndef EVAL_H_
#define EVAL_H_

#include "lexer.h"

enum class EvalStatus {
  ok, empty, error, eof
};

struct EvalResult {
  const EvalStatus status;
  const int result;
};

class Eval {
  public:
    Eval(Lexer& lexer) :
      lexer_(lexer), token_(lexer.next_token()) {}
    const EvalResult check_init_token(TokenKind);
    const EvalResult evaluate(TokenKind = TokenKind::nl);
  private:
    Lexer &lexer_;
    Token token_;   //lookahead token
};

#endif //ifndef EVAL_H_
