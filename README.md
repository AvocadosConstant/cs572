# CS572: Compiler Design

Binghamton University | Prof. Zerksis D. Umrigar | Fall 2017

## Projects

### - [Parenthesized Sub-Expressions for a Basic Calculator Language](./submit/prj1-sol/)
Extend a tiny calculator language to support parenthesized sub-expressions.

### - [Pretty Printer](./submit/prj2-sol/)
Pretty print programs written in decaf, a subset of java.

### - [Code Generator](./submit/prj4-sol/)
Generate jasmine and .class files from .decaf programs.
